#!/bin/bash
if [ -z $1 ]
then
    echo "Gets Vbox Guest Additions"
    echo "Usage:"
    echo "vboxget.sh X.X.X"
    echo "X.X.X - version"
else
    VBOX_VER=$1
    echo Trying to get Virtual Box Additions: $VBOX_VER
    wget -c http://download.virtualbox.org/virtualbox/${VBOX_VER}/VBoxGuestAdditions_${VBOX_VER}.iso -P ~/tmp
    echo Updating apt sources
    sudo apt-get update -y
    echo Trying to install kernel headers
    sudo apt-get install build-essential linux-headers-`uname -r` dkms -y
    echo Trying to install xorg server
    sudo apt-get install xserver-xorg xserver-xorg-core -y
    echo Checking if gcc is installed
    gcc --version 
    if [ $? -ne 0 ]
    then
	echo Installing gcc
	sudo apt-get install gcc -y
    fi
    echo Checking if make is installed
    make --version
    if [ $? -ne 0 ]
    then 
	echo Installing make 
        sudo apt-get install make -y
    fi 
    echo Trying to mount iso image
    mkdir ~/tmp/vbox
    sudo mount -o loop ~/tmp/VBoxGuestAdditions_${VBOX_VER}.iso ~/tmp/vbox
    echo Trying to run installer
    sudo ~/tmp/vbox/VBoxLinuxAdditions.run
    echo Unmounting directory
    sudo umount ~/tmp/vbox
    rmdir ~/tmp/vbox
fi


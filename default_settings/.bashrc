# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions
#echo -n "Should i use SCREEN terminal manager? (y/n)"
#read ans
#if [ $ans == "y" ] ; then
#  screen
#  break
#else 
#  echo -n "Should i try to connect to SCREEN session? (y/n)"
#  read ans
#  if [ $ans == "y" ] ; then 
#    screen -D -RR
#  fi
#fi

if [ -f `which powerline-daemon` ]; then
  powerline-daemon -q
  POWERLINE_BASH_CONTINUATION=1
  POWERLINE_BASH_SELECT=1
  . /usr/share/powerline/bash/powerline.sh
fi

export PATH=${PATH}:~/bin:~/lib:~/include

screen -d -RR
screenfetch

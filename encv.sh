#!/bin/bash

if [ "$1" == "" ]; then
    echo "encv.sh <filename> [time_from] [time_to]"
    exit 1
fi

INFILE="$1"
OUTEXT="${INFILE##*.}"
OUTFILE="${INFILE%.*}-out.$OUTEXT"
BITRATE="350k"

SETBR=0

if [ "$2" == "" ]; then 
    FROM=""
elif [ "$2" == "-b" ]; then 
    SETBR=1
    FROM=""
else 
    FROM="-ss $2"
fi

if [ "$3" == "" ]; then
    TO=""
    if [ $SETBR -eq 1 ]; then
       echo Error setting bitrate
       exit 2
    fi 
else
    if [ $SETBR -eq 1 ]; then
       BITRATE="$3"
       TO=""
    else 
       TO="-to $3"
    fi
fi

if [ $SETBR -eq 1 ]; then
    if [ "$4" == "" ]; then
        FROM=""
    else
        FROM="-ss $4"
    fi
    if [ "$5" == "" ]; then
        TO=""
    else
        TO="-to $5"
    fi
fi

echo Filename: $INFILE
echo Output filename: $OUTFILE
echo Set bitrate: $SETBR
echo Bitrate: $BITRATE
echo From: $FROM
echo To: $TO
echo ""

ffmpeg -y $FROM -i "$INFILE" $TO -c:v libx265 -b:v $BITRATE -x265-params pass=1 -an -f null /dev/null && \
ffmpeg $FROM -i "$INFILE" -to 04:05:29 -c:v libx265 -b:v $BITRATE -x265-params pass=2 -c:a copy $OUTFILE

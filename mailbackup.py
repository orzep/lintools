#!/usr/bin/env python3

# author: Marcin Orzepowski

import base64
# ftp without TLS
#from ftplib import FTP
# ftp with TLS
from ftplib import FTP_TLS as FTP
import glob
import grp
from lintools import make_dir
import os
import pwd
import subprocess
import tarfile
import time
from io import BytesIO

#user private settings in conf_private.py module not sychronized by git
#FTP_PWD should be declared by user in conf_private.py in base64 encoding
#FTP_PWD = 
try:
    from conf_private import BACKUP_USER, BACKUP_GRP, FTP_PWD
    ftp_configured = True
except ImportError:
    print("There is no ftp password configured")
    print("Please create file conf_private.py and add FTP_PWD=b'<password>'")
    print("where <password> is password in base64 encoding")
    ftp_configured = false
try:
    from conf_private import DB_USER, DB_PWD
except ImportError:
    print("There is no db password configured")
    print("Please create file conf_private.py and add DB_PWD=b'<password>'")
    print("Please add DB_USER='<username>'")
    print("where <password> is password in base64 encoding")
    
#other typical settings

VERSION = (2,0,1)

uid = pwd.getpwnam(BACKUP_USER).pw_uid
gid = grp.getgrnam(BACKUP_GRP).gr_gid
user_dirs = next(os.walk("/home"))[1]
user_dirs.append("root")
HOME_DIR = "/home"
BACKUP_DIR = "backup/mail"
BACKUP_DIR = HOME_DIR + "/" + BACKUP_USER + "/" + BACKUP_DIR 
MAIL_DIR = "Mail"
SPOOL_DIR = "/var/spool/mail"
MAIL_BACKUP_PREFIX = "-mail-"
SPOOL_BACKUP_PREFIX = "-spool-"
DB_BACKUP_PREFIX = "mysqldb-"
TIME_SUFFIX = str(time.time()).replace('.', '')
ARCHIVE_SUFFIX = ".tar.xz"
BACKUPS_NUM = 3
FTP_SERVER = "10.10.10.4"
DB_DUMP_CMD = "mysqldump"
DB_DUMP_OPTIONS = [DB_DUMP_CMD,
    "--user={usr}".format(usr=DB_USER),
    "--password={pwd}".format(pwd=bytes.decode(base64.b64decode(DB_PWD))),
    "--all-databases"]

FTP_FILENAME_SUFFIX = "-latest"
FTP_DIR = "/usr/local/download/back"


def create_backup():
    print("Trying to create backup dir")
    make_dir(BACKUP_DIR)
    os.chown(BACKUP_DIR, uid, gid)
    last_backup_files = list()
    for user in user_dirs:
        mail_archive_name = BACKUP_DIR + "/" + user + MAIL_BACKUP_PREFIX  + \
             TIME_SUFFIX + ARCHIVE_SUFFIX
        spool_archive_name =  BACKUP_DIR + "/" + user + SPOOL_BACKUP_PREFIX  + \
             TIME_SUFFIX + ARCHIVE_SUFFIX
        rem_flag = False
        if user == "root":
            add_path = "/root/" + MAIL_DIR
        else:
            add_path = HOME_DIR + "/" + user + "/" + MAIL_DIR
        try:
            print("Creating: " + mail_archive_name )
            tar = tarfile.open(mail_archive_name, "w:xz")
            tar.add(add_path)
            last_backup_files.append( mail_archive_name)
        except FileNotFoundError:
            print("Mail directory not found for user: " + user)
            rem_flag = True
        finally:
            tar.close()
            os.chown(mail_archive_name, uid, gid)
            if rem_flag:
                os.remove(mail_archive_name)
        rem_flag = False
        add_path = SPOOL_DIR + "/" + user
        try:
            print("Creating: " + spool_archive_name )
            tar = tarfile.open(spool_archive_name, "w:xz")
            tar.add(add_path)
            last_backup_files.append( spool_archive_name)
        except FileNotFoundError:
            print("Spool file not found for user: " + user)
            rem_flag = True
        finally:
            tar.close()
            os.chown(spool_archive_name, uid, gid)
            if rem_flag:
                os.remove(spool_archive_name)
    db_back_fname = BACKUP_DIR + "/" + DB_BACKUP_PREFIX + \
        TIME_SUFFIX +ARCHIVE_SUFFIX    
    db_backup(db_back_fname)
    last_backup_files.append(db_back_fname)
    return(last_backup_files)

        
def remove_old_backups(keyvalue):
    backup_files = glob.glob(BACKUP_DIR + "/*" + keyvalue + "*" + ARCHIVE_SUFFIX)
    files_dict = dict()
    for backup_file in backup_files:
        key = backup_file.split(keyvalue)[0]
#        print("{0} : {1}".format(key, backup_file))
        try:
            files_dict[key] 
        except KeyError:
            files_dict[key] = list()
        finally:
            files_dict[key].append(backup_file)
    if len(files_dict[key]) <= BACKUPS_NUM:
        print("No files to remove")
        return
#    print(files_dict)
    for entry in files_dict:
       backups_list = sorted(files_dict[entry], reverse=True)
       to_remove = backups_list[BACKUPS_NUM:]
       for del_file in to_remove:
           print("Deleting old backup: " + del_file)
           os.remove(del_file)


def send_last_backup(last_backup_files):
    ftp_pass = bytes.decode(base64.b64decode(FTP_PWD))
    ftp = FTP(FTP_SERVER, user=BACKUP_USER, passwd=ftp_pass)
    ftp.set_pasv(True)
# comment below for ftp without TLS
    ftp.prot_p()
#    ftp.login()
    ftp.cwd(FTP_DIR)
    for file in last_backup_files:
        ftp_path, ftp_filename = os.path.split(file)
        temp_name = ftp_filename.rsplit("-", 1)
        ftp_filename = temp_name[0] + FTP_FILENAME_SUFFIX + ARCHIVE_SUFFIX
        ftp.storbinary("STOR " + ftp_filename, open(file, "rb"), 1024)
        print("Sending file: {0} to host {1}".format(ftp_filename, FTP_SERVER))

        
def db_backup(db_back_fname):
    dbdump_proc = subprocess.Popen(DB_DUMP_OPTIONS, stdout=subprocess.PIPE)
    print("Creating database backup. Please wait...")
    #TODO: error checking
    outs, errs = dbdump_proc.communicate()
    print("Compressing database data. Please wait...")
     
    tarinfo = tarfile.TarInfo(db_back_fname[:-len(ARCHIVE_SUFFIX)])
    tarinfo.size = len(outs)

    with tarfile.open(db_back_fname, 'w:xz') as tar:
        tar.addfile(tarinfo, BytesIO(outs))
        
    print("Database backup {0} created".format(db_back_fname))
    
if __name__ == "__main__":
    last_backup_files = create_backup()
    remove_old_backups(MAIL_BACKUP_PREFIX)
    remove_old_backups(SPOOL_BACKUP_PREFIX)
    remove_old_backups(DB_BACKUP_PREFIX)
    if ftp_configured == True:
       send_last_backup(last_backup_files)

#!/usr/bin/env python3
import urllib.request
import smtplib
try:
    from conf_private import MYIP_USER, MYIP_PWD, MYIP_SERVER
except ImportError:
    print("There is no login and password configured")
    print("Please create file conf_private.py and add:")
    print("MYIP_USER='<username>'")    
    print("MYIP_PWD='<password>'")
    print("MYIP_SERVER='<server:port>'")
    print("where <password> is password in base64 encoding")
    exit(1)

Request = urllib.request.Request('http://ipinfo.io', headers={'User-Agent': 'curl'})
with urllib.request.urlopen(Request) as response:
    the_page = response.read().decode("utf8")

    print(the_page)

    server = smtplib.SMTP(MYIP_SERVER)
    server.ehlo()
    server.starttls()
    server.login(MYIP_USER, MYIP_PWD)
    message = "\r\n".join([
        "From: {0}".format(MYIP_USER),
        "To: {0}".format(MYIP_USER),
        "Subject: IP configuration",
        "\r\n"
        "Wellcome to orzep server\r\n"
        "Current ip configuration is:\r\n",
        the_page,
        "\r\n"
    ])

    server.sendmail(MYIP_USER, MYIP_USER, message)

#!/bin/bash
IDEA_VERSION=2016.3.3
PYCHARM_VERSION=2016.3.2
#FREE=community-
JDK=-no-jdk
#for wersion with jdk use
#JDK=

#for full version use
FREE=
echo "------------------------------------------------------------------------"
echo "|                    Installing IntelliJ IDEA                          |"
echo "------------------------------------------------------------------------"
if [ -z $FREE ]; then
    IDEANAME=ideaIU
else
    IDEANAME=ideaIC
fi
wget -O ~/intellij.tar.gz https://download.jetbrains.com/idea/${IDEANAME}-${IDEA_VERSION}${JDK}.tar.gz &&
tar zxvf ~/intellij.tar.gz

echo "------------------------------------------------------------------------"
echo "|                    Installing PyCharm                                |"
echo "------------------------------------------------------------------------"
wget -O ~/pycharm.tar.gz https://download-cf.jetbrains.com/python/pycharm-${FREE}${PYCHARM_VERSION}.tar.gz &&
tar zxvf ~/pycharm.tar.gz

#!/usr/bin/python -tt
# skvidal@fedoraproject.org
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# Copyright 2009 Red Hat Inc 
# Written by Seth Vidal
# process kernel and kernel-smp packages and hand back n-v-r info on
# the oldest ones barring the running kernel and the latest one


import rpm
import sys
import os


def get_running_kernel_package_info():
    ver = os.uname()[2]
    kernfile = '/boot/vmlinuz-%s' % ver
    ts = rpm.TransactionSet()
    mi = ts.dbMatch('basenames', kernfile)
    num = mi.count()
    if num == 0:
        return None
    else:
        hdr = mi.next()
        return (hdr['name'], hdr['version'], hdr['release'])

def sorthdrs(hdr1, hdr2):
    rc = rpm.versionCompare(hdr1, hdr2)
    if rc < 0:
        return -1
    elif rc == 0:
        return 0
    elif rc > 0:
        return 1


def main():
    runname, runver, runrel = get_running_kernel_package_info()
    runstring = '%s-%s-%s' % (runname, runver, runrel)
    ts = rpm.TransactionSet()
    numkernels = 2
    if len(sys.argv) > 1:
        numkernels = int(sys.argv[1])
    oldlist = []
    for pkgname in ['kernel', 'kernel-smp', 'kernel-enterprise'
                    'kernel-hugmem', 'kernel-bigmem', 'kernel-PAE', 'kernel-xen']:
        mi = ts.dbMatch('name', pkgname)
        thislist = []
        if mi.count() > 1:
            kernlist = []
            for hdr in mi:
                thislist.append(hdr)
            thislist.sort(sorthdrs)
            num = numkernels*-1
            if num:
                thislist = thislist[:num]
            for hdr in thislist:
                kstring = '%s-%s-%s' % (hdr['name'], hdr['version'], hdr['release'])
                if kstring != runstring:
                    oldlist.append(kstring)
       
    # get the list of all the older kernels
    # iterate them and remove our running kernel
    # return n-v-r on the line for handing off to yum

    for item in oldlist:
        print ' %s' % item
    
    
if __name__ == "__main__":
    main()
    

#!/usr/bin/python3 
import os
import signal
import subprocess
import sys
import time



def del_file(file_path):
    if os.path.isfile(file_path):
        print("   Removing file with zeros...")
        os.remove(file_path)
        print("Disk is zeroed")




try:
    empty_file = sys.argv[1]
except IndexError:
    print("Zeroes empty space in directory")
    print("Usage:")
    print("zerof.py /path/to/file")
    exit(1)

dd_proc = subprocess.Popen(["dd", "if=/dev/zero", ("of=" + empty_file),
    "bs=32M"])
time.sleep(1.5)
looping = True
while(looping):
    try:
        ret = dd_proc.poll()
        if ret is not None:
            print("ret: " + str(ret))
            if int(ret) == 1:
                print("Free space is full with zeros...")
                looping = False
        dd_proc.send_signal(signal.SIGUSR1)
        time.sleep(2.5)
    except KeyboardInterrupt:
        print("Program stopped")
        dd_proc.send_signal(signal.SIGKILL)
        del_file(empty_file)
        sys.exit()
    except ProcessLookupError:
        break


del_file(empty_file)
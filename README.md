# Linux admin tools #
## Installation ##
*  Clone repository 

```
#!shell

git clone https://orzep@bitbucket.org/orzep/lintools.git
```

* Move lintools catalog as bin

```
#!shell

mv ./lintools ~/bin
```
* Add path to bin directory


```
#!shell

echo 'PATH=$PATH:~/bin' >> ~/.bashrc
echo 'export PATH' >> ~/.bashrc
```
* Enjoy
##Using runvnc 
###Debian/Ubuntu
* Install needed packages
```
#!shell

sudo apt-get install dialog tightvncserver autocutsel -y 
```

*Add to ~/.vnc/xstartup following linses before 
```
#!shell
# Makes copy/past work - must come before RANDR 'fix'
#vncconfig -iconic &
autocutsel -s CLIPBOARD -fork
autocutsel -s PRIMARY -fork

```

### Red-Hat/Fedora
* Install needed packages
dnf is now default - if wanted use yum
```
#!shell
sudo dnf install dialog tigervnc tigervnc-server 

```
## Fancy bash banner
* Install needed packages
###Debian/Ubuntu

```
#!shell

sudo apt-get install screenfetch
```
###Red-Hat/Fedora
```
#!shell

sudo dnf install screenfetch
```
* Add following line to .bashrc

```
#!shell

screenfetch
```

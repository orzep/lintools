#!/bin/bash

echo "Upgrading software"
sudo apt-get update -y && sudo apt-get upgrade -y && sudo apt-get dist-upgrade -y
sudo apt-get auto-remove -y
echo "Installing git vcs"
sudo apt-get install git -y
echo "Installing essential build for virtualbox modules"
sudo apt-get install build-essential dkms linux-headers-`uname -r` -y
echo "Installing X11"
sudo apt-get install xorg -y
echo "Installing virtualbox guest additions"
sudo apt-get install virtualbox-guest-utils virtualbox-guest-dkms virtualbox-guest-x11 -y
echo "Installing mysql server"
sudo apt-get install mysql-server libpam-mysql -y
echo "Installing postgresql server"
sudo apt-get install postgresql libpam-pgsql-y
echo "Installing php"
sudo apt-get install php php-mysql php-bz2 php-zip
echo "Installing apache2"
sudo apt-get install apache2 libapache2-mod-wsgi libapache2-mod-uwsgi libapache2-mod-php -y
sudo a2enmod proxy_http authz_groupfile ssl
echo "Installing smpt and imap and webmail"
sudo apt-get install postfix dpostfixadmin dovecot-core dovecot-mysql dovecot-pgsql dovecot-imapd vmm vmm-doc -y
sudo apt-get install roundcube roundcube-mysql roundcube-pgsql roundcube-plugins roundcube-plugins-extra roundcube-sqlite3
echo "Installing ftp"
sudo apt-get install proftpd -y
echo "Installing vnc"
sudo apt-get install tightvncserver -y
echo "Installing mc"
sudo apt-get install mc -y
echo "Installing xfce"
sudo apt-get install xfce4 xfce4-whiskermenu-plugin xfwm4-themes gtk3-engines-xfce lxappearance gnome-icon-theme tango-icon-theme -y
echo "Installing screenfetch"
sudo apt-get install screenfetch -y
echo "Installing webalizer"
sudo apt-get install webalizer -y
echo "Installing lm-sensors"
sudo apt-get install lm-sensors -y
echo getting LinTools from bitbucket
git clone https://orzep@bitbucket.org/orzep/lintools


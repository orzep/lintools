#! /usr/bin/env python3
import sys
import os
from os import path

CL = {
"CRITICAL": "\033[91m",
"ERROR": "\033[31m",
"WARNING": "\033[93m",
"INFO": "\033[32m",
"DEBUG": "\033[35m",
"NOTSET": "\033[37m",
"RESET": "\033[0m" }


def cl_print(msg, color):
    message = color + msg + CL["RESET"] + "\n"
    sys.stdout.write(message)

if len(sys.argv) != 2:
    print ("Prints logs in color\n  Usage:\n")
    print ("  logcolor <filename>")
    exit(1)

filename = sys.argv[1]
with open(filename) as f:
    data = f.read()
    lines = data.splitlines()
    for line in lines:
        for level in CL:
            if line.find(level) > 0:
                cl_print(line, CL[level])
                break;
    
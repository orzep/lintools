#!/usr/bin/python3
import os
import re
import shutil 
import subprocess as sp

home_dir = os.path.expanduser("~")


def make_dir(name):
    if not os.path.exists(name):
        os.makedirs(name)


def install_packages():
    result = sp.run(["dpkg", "-l"], stdout=sp.PIPE)
#    print(bytes.decode(result.stdout, "utf-8"))
    check_dialog = re.findall(r"^ii  dialog", bytes.decode(result.stdout, "utf-8"), re.MULTILINE)
    if len(check_dialog) == 0:
        sp.run(["sudo", "apt-get", "install", "dialog", "screenfetch", "-y"])




def bashrc_manip():
    global home_dir
    out_content = list()
    #read bashrc
    bashrc_file = open(home_dir + "/.bashrc", "r")
    adding = True
    #bashrc = bashrc_file.readlines()
    for line in bashrc_file:
       out_content.append(line)
       if line.find("force_color_prompt") != -1 and adding:
           print("Adding color mode to bash")
           out_content.append("force_color_prompt=yes\n")
           adding = False
    bashrc_file.close()
    print("Adding local path")
    out_content.append("PATH=$PATH:$HOME/bin:$HOME/lib:$HOME:/include\n")
    out_content.append("screenfetch\n")
    bashrc_file = open(home_dir + "/.bashrc", "w")
    print("Wrinting bash configuration")
    bashrc_file.writelines(out_content)
    bashrc_file.close()


def copytree(src, dst, symlinks=False, ignore=None):
    if not os.path.exists(dst):
        os.makedirs(dst)
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            copytree(s, d, symlinks, ignore)
        else:
            if not os.path.exists(d) or os.stat(s).st_mtime - os.stat(d).st_mtime > 1:
                shutil.copy2(s, d)

def main():
    global home_dir

    #create directories
    for dir_name in ["bin", "lib", "include", "src", "tmp"]:
        ins_dir = home_dir + "/" + dir_name
        print("Creating directory" + ins_dir)
        make_dir(ins_dir)
    #change bashrc
    bashrc_manip()
    #copy lintools
    lintools_path = os.path.dirname(os.path.realpath(__file__))
    print("Copying directory contents to {0}".format(home_dir  + "/bin"))
    copytree(lintools_path, home_dir + "/bin")
    install_packages()



if __name__ == "__main__":
    main()

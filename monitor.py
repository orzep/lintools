#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import sys
import subprocess as sps
import time
from cpus import *

def header():
    print("   CPU0      CPU1       SPEED0        SPEED1")




def main():
    i = 0
    header()
    try:
        while True:
#            if (i % 23) == 0:
            p = sps.Popen("sensors", stdout=sps.PIPE)
            output, err = p.communicate()
            outstr = output.decode("utf-8")
            cpu_idx = outstr.find("Core 0:") + len("Core 0:")
            cpu0 = outstr[cpu_idx:]
            cpu_idx = cpu0.find("°C")
            cpu0 = cpu0[:cpu_idx]
            cpu0 = cpu0.strip()
            cpu_idx = outstr.find("Core 1:") + len("Core 1:")
            cpu1 = outstr[cpu_idx:]
            cpu_idx = cpu1.find("°C")
            cpu1 = cpu1[:cpu_idx]
            cpu1 = cpu1.strip()
            print("  " + cpu0 + "      " + cpu1 + 
                "     " + get_cpu_speed(0) + 
                "      " + get_cpu_speed(1))
            time.sleep(2)
            i += 1
    except KeyboardInterrupt:
            print("\rExiting")
#            print (sys.exc_info()[0])
#    return 0


if __name__ == "__main__":
    main()

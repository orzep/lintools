#!/usr/bin/python3

print("sertemp - prints raspberry temperature over serial port")
print("sertemp [SERIAL_PORT]")
print("if SERIAL_PORT is not given /dev/ttyAMA0 is used")

import serial
from serial import Serial
import subprocess
import sys
import time


VC_DIR = "/opt/vc/bin"
VCGENCMD = VC_DIR + "/vcgencmd"
BITRATE = 115200
BITS = 8
PARITY = serial.PARITY_NONE
STOPBITS = serial.STOPBITS_ONE
TERMINATOR = b"\r"
SLEEP_TIME = 2.0

try:
    device = sys.argv[1]
except IndexError:
    device = "/dev/ttyAMA0"
try:
    ser_port = Serial(
        device,
        baudrate=BITRATE,
        parity=PARITY,
        stopbits=STOPBITS,
        bytesize=BITS
        )
    # not needed in my installation
#ser_port.open()
except serial.serialutil.SerialException as e:
    print(e)
    exit()
ser_port.write(b"Raspberry Pi Connected" + TERMINATOR)

try:
    while True:
        vcproc = subprocess.Popen([VCGENCMD, "measure_temp"],
            stdout=subprocess.PIPE)
        vcproc.wait()
        while True:
            line = vcproc.stdout.readline()
            if len(line) != 0:
                print (line.decode("UTF-8").rstrip())
                ser_port.write(line + TERMINATOR)
            else:
                break
        time.sleep(SLEEP_TIME)
except KeyboardInterrupt:
    ser_port.write(b"Disconnecting...\n" + TERMINATOR)
    print("Disconnecting...")
    ser_port.close()
    exit()

ser_port.close()
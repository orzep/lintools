#!/usr/bin/env python3
import subprocess as sub
import re

cpuinfopath = '/proc/cpuinfo'

def cpuinfo():
	""" returns values and total number of cpus
	"""
	result=list()
	cpus = 0
	cpufile = open(cpuinfopath, 'r')
	for line in cpufile:
		find1 = line.find('model name')
		find2 = line.find('cpu MHz')
		if find1 != -1 or find2 != -1 :
			out = line.strip()
		#	print out
			result.append(line)
			if line.find("ARMv6") != -1:
				sub.Popen(["/opt/vc/bin/vcgencmd",
				    "measure_clock", "arm"])
			cpus += 1
	result.append(cpus/2)
	return result
#	pat1 = re.compile('model name')
#	pat2 = re.compile('cpu MHz')
#	for line in cpufile:
#		m = re.search(pat1, line)
#		if m != None:
#			print m.string	
#		m = re.search(pat2, line)
#		if m != None:
#			print m.string
		

def get_cpu_speed(cpu):
	tab = cpuinfo()
	cores = tab[-1]
	if (cpu < 0) or (cpu >= cores):
		return -1
	cpu *= 2
	cpu += 1
	line = tab[cpu]
	line = line[line.find(":")+1:].strip()
	return line		
	#return tab[cpu]

def main():
	x = cpuinfo()
	for x in cpuinfo()[:-1]:
		print(x[:-1])

if __name__ == "__main__":
	main()
